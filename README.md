<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
# Table of Contents

- [Table of contents](#table-of-contents)
- [Disclaimer](#disclaimer)
- [Introduction](#introduction)
- [Idées](#idées)
    - [Métier de chercheur•se](#métier-de-chercheurse)
        - [À qui ressemble un chercheur?](#à-qui-ressemble-un-chercheur)
        - [Comment devenir chercheur•se en informatique?](#comment-devenir-chercheurse-en-informatique)
        - [Qu'est-ce qu'on fait de nos journées?](#quest-ce-quon-fait-de-nos-journées)
        - [Pourquoi on cherche en informatique?](#pourquoi-on-cherche-en-informatique)
        - [Qu'est-ce qu'on fait de nos recherches?](#quest-ce-quon-fait-de-nos-recherches)
        - [Est-ce qu'on travaille tout•e seul•e?](#est-ce-quon-travaille-toute-seule)
        - [Est-ce qu'on ne fait que de la recherche?](#est-ce-quon-ne-fait-que-de-la-recherche)
    - [Thèse de doctorat](#thèse-de-doctorat)
        - [Qu'est-ce qu'une thèse?](#quest-ce-quune-thèse)
        - [Qu'est-ce qu'un•e doctorant•e?](#quest-ce-quune-doctorante)
    - [Équipe de recherche](#équipe-de-recherche)
        - [Qu'est-ce qu'une équipe de recherche?](#quest-ce-quune-équipe-de-recherche)
        - [Est-ce que tous les membres de l'équipe ont le même parcours?](#est-ce-que-tous-les-membres-de-léquipe-ont-le-même-parcours)
    - [Laboratoire de recherche](#laboratoire-de-recherche)
        - [Qu'est-ce qu'un laboratoire de recherche?](#quest-ce-quun-laboratoire-de-recherche)
        - [Comment est organisé un laboratoire de recherche?](#comment-est-organisé-un-laboratoire-de-recherche)
    - [Campus universitaire](#campus-universitaire)
        - [Quels bâtiments trouve-t-on sur un campus?](#quels-bâtiments-trouve-t-on-sur-un-campus)
        - [Est-ce qu'il n'y a que l'université?](#est-ce-quil-ny-a-que-luniversité)
    - [Enseignement](#enseignement)
        - [Comment se passe un cours?](#comment-se-passe-un-cours)
        - [Qu'est-ce qu'on apprend?](#quest-ce-quon-apprend)
        - [Est-ce qu'on sait tout?](#est-ce-quon-sait-tout)
    - [Thématiques de recherche](#thématiques-de-recherche)
        - [Quelles thématiques sont en cours d'investigation?](#quelles-thématiques-sont-en-cours-dinvestigation)
        - [Comment valide-t-on nos recherches?](#comment-valide-t-on-nos-recherches)
        - [Est-ce qu'on ne travaille que sur un sujet?](#est-ce-quon-ne-travaille-que-sur-un-sujet)
    - [Exemple de planning sur une semaine](#exemple-de-planning-sur-une-semaine)
- [Backup](#backup)
    - [Sites de programmation](#sites-de-programmation)
    - [Activités de recherche guidée](#activités-de-recherche-guidée)
    - [Sites de vulgarisation](#sites-de-vulgarisation)
    - [Sites de présentation des métiers du numérique](#sites-de-présentation-des-métiers-du-numérique)
    - [Sites pour des ateliers d'informatique débranchée](#sites-pour-des-ateliers-dinformatique-débranchée)
    - [Livret à donner](#livret-à-donner)
- [Remarques finales](#remarques-finales)

<!-- markdown-toc end -->


# Disclaimer
Cette page est une tentative pour rassembler des idées d'activités
à faire lors de l'accueil de stagiaires de 3ème. Ces stages sont des
stages **d'observation** pour les élèves de 3ème ou 2nde. Ils n'ont pas
pour but de mener les stagiaires à une réalisation pratique. Donc
ci-dessous des idées de sujets à discuter pour présenter la recherche,
le métier de chercheur et l'environnement de travail. Cette liste n'est
pas :
- exhaustive 
- un programme à suivre entièrement pour chaque stagiaire.

# Introduction
Il peut être intéressant de demander en préambule au stagiaire :
- ce qu'il ou elle attend de son stage
- ce qu'il ou elle aimerait découvrir, savoir en repartant
- ce qu'il ou elle doit rendre à leur établissement (rapport, etc.)
- qui il ou elle aimerait rencontrer durant son stage

Le site qui explique ce qu'est un stage de 3e :  
http://www.education.gouv.fr/cid107211/le-stage-de-3e.html  

# Idées
## Métier de chercheur•se

### À qui ressemble un chercheur?
- pour briser la glace, demander les représentations clichées qu'ont les stagiaires
<img src="Tournesol.jpg" width="200"/>
<img src="Doc.jpg" width="300"/>

- une image de chercheuse?
- 3 filières dans la recherche : chercheur•se, enseignant-chercheur, ingénieur•e en R&D

### Comment devenir chercheur•se en informatique?
- présenter les études pour y arriver
- poster de présentation des études
  ![](https://wiki.inria.fr/wikis/sciencinfolycee/images/9/98/Les_métiers_de_la_recherche_en_ISN_%28visuel%29.png)
- portraits d'informaticiennes et informaticiens célèbres  
https://www.histoire-informatique.org/portraits/  
http://blog.iakaa.com/ces-10-femmes-qui-ont-change-linformatique/  
https://interstices.info/wp-content/uploads/2019/01/Notice-web.pdf

### Qu'est-ce qu'on fait de nos journées?
- détailler le planning sur une semaine
- assister aux réunions de la semaine (réunions de projet, de service,
  d'équipe, de comité, de discussion, de brainstorming, etc.)
- poster de présentation de la recherche en informatique
  ![](https://wiki.inria.fr/wikis/sciencinfolycee/images/6/67/Les_métiers_de_la_recherche_en_ISN_%28poster%29.jpg)
- livret de présentation du poster pour l'encadrant•e   
  https://wiki.inria.fr/wikis/sciencinfolycee/images/9/9a/Les_métiers_de_la_recherche_en_ISN_%28livret_pour_l_intervenant%29.pdf  

### Pourquoi on cherche en informatique?
- trouver quelque chose ou prouver que ce n'est pas possible
- assister à un séminaire d'équipe
- film (23mn environ) + livret explicatif  
  http://sparticipatives.gforge.inria.fr/film/index.php  

### Qu'est-ce qu'on fait de nos recherches?
- expliquer le système de conférences/journaux et de reviews
- regarder le site web d'une conférence de notre domaine (contraintes
  sur les papiers soumis, format du programme, comité d'organisation,
  etc.)
- demander à un•e doctorant•e de présenter son dernier poster ou la
  publication dont il ou elle est la plus fière en expliquant pourquoi
- regarder une publication 'importante'  
  https://fr.wikipedia.org/wiki/Liste_de_publications_importantes_en_informatique  

### Est-ce qu'on travaille tout•e seul•e?
- assister à une réunion de projet et/ou une réunion hebdomadaire avec
  un•e doctorant•e
- parler de collaborations internationales

### Est-ce qu'on ne fait que de la recherche?
- parler d'enseignement et/ou d'activités autres (séminaires,
  vulgarisation, comités de recrutement, dépôt de projets, transferts
  industriels, etc.)
- docu-fiction sur le cheminement du travail de recherche scientifique (33 minutes)  
  https://www.canal-u.tv/video/inria/avis_de_recherche.7546

## Thèse de doctorat
### Qu'est-ce qu'une thèse?
- assister à une soutenance
- expliquer le rôle du jury, des rapporteurs et des encadrants
- montrer un manuscrit de thèse

### Qu'est-ce qu'un•e doctorant•e?
- assister à une réunion doctorant•e / encadrants
- assister à un TP/TD donné par un•e doctorant•e

## Équipe de recherche
### Qu'est-ce qu'une équipe de recherche?
- faire le tour des bureaux (sans oublier l'assistant•e de l'équipe)
- détailler les différents employeurs des membres de l'équipe

### Est-ce que tous les membres de l'équipe ont le même parcours?
- montrer la diversité des parcours et nationalités
- prendre un café avec le reste de l'équipe à la cafet

## Laboratoire de recherche
### Qu'est-ce qu'un laboratoire de recherche?
- faire le tour des bâtiments (cluster, cafet, etc.)

### Comment est organisé un laboratoire de recherche?
- parler des différentes équipes, départements, instituts
- l'organigramme de l'IRISA
  http://www.irisa.fr/sites/default/files/2021-02/organigrammeIRISA_fr_202102.pdf 

## Campus universitaire
### Quels bâtiments trouve-t-on sur un campus?
- faire un tour du campus (bibliothèque, Resto U, ISTIC, etc.)
- montrer un amphi
- plan du campus de Beaulieu à Rennes  
  https://www.univ-rennes1.fr/sites/www.univ-rennes1.fr/files/asset/document/univ_rennes_1_-_plan_beaulieu_-_iut.pdf 

### Est-ce qu'il n'y a que l'université?
- présenter les différentes écoles, UFR, IUT, etc.
- montrer d'autres labos (OSUR, etc.)

## Enseignement
### Comment se passe un cours?
- assister à un cours
- assister à des soutenances de projets d'étudiants
- faire un tour dans une salle d'exam pendant un exam

### Qu'est-ce qu'on apprend?
- visiter une salle TP pendant un TP
- regarder un sujet de TD ou TP de L1 par exemple
- discuter avec un•e stagiaire de M2 sur son parcours, l'intérêt du
  stage dans sa formation, etc.
- regarder la plaquette d'un parcours
- enseignements en L2 et L3 informatique à l'ISTIC  
  https://istic.univ-rennes1.fr/les-enseignements-en-l2-et-l3-parcours-informatique  

### Est-ce qu'on sait tout?
- expliquer les formations, écoles de recherche
- faire un tour dans une formation SED
- raconter comment on construit un nouveau cours ou une nouvelle formation

## Thématiques de recherche
### Quelles thématiques sont en cours d'investigation?
- rencontrer des collègues de l'équipe (doctorant•e, post-doctorant•e,
  ingénieur•e, stagiaire de M2, etc.)
- exemples de questions à leur poser :
   + parcours scolaire et après
   + sujet de recherche
   + occupation au quotidien
   + pourquoi avoir choisi ce travail

### Comment valide-t-on nos recherches?
- présenter une plate-forme d'expérimentation
- parler de logiciels de simulation
- expliquer comment on peut prouver un algorithme

### Est-ce qu'on ne travaille que sur un sujet?
- présenter les différentes thématiques auxquelles on s'est intéressé
  et leur évolution au cours du temps
- rencontrer des personnes d'une autre équipe (préférablement sur un
  sujet éloigné)

## Exemple de planning sur une semaine
- voici un exemple de planning pour occuper des stagiaires pendant une
semaine (avec d'autres idées comme des échanges de stagiaires entre
équipes) : [ici](Programme_de_la_semaine_de_stage.pdf)
- pour d'autres idées, un super livret réalisé dans le secteur
du BTP pour *bien accueillir un élève en stage de 3e dans votre
entreprise*. : [ici](7livretentreprise.pdf) 

# Backup
## Sites de programmation

Voici quelques sites en ligne premettant de mettre les stagiaires en
autonomie complète, à condition qu'ils aient un ordinateur. Parfait
pour qu'ils ne s'ennuient pas pendant votre réunion indéplaçable à
laquelle ils ne peuvent pas assister.

- Le [concours castor](http://castor-informatique.fr/): destiné aux
  collégiens et lycéens, c'est un ensemble de petits casse-têtes très
  bien faits sur la pensée informatique. On trouve pour chaque année des
  suites de questions dimensionnées pour être faites en 45mn, la fin
  de l'heure étant généralement dédiée à la lecture de la correction.
  C'est vraiment très bien fait, avec des questions bien choisies et des
  réponses instructives au sujet des concepts de l'informatique.
- [Algorea](http://algorea.org/#/): Assez proche du concours castor
  puisque fait par les mêmes gens, mais plus dédié à l'algorithmique.
- Le [concours AlKindi](http://www.concours-alkindi.fr/): Encore une
  déclinaison intéressante du concours castor, au sujet de la sécurité
  informatique cette fois-ci.
- [Code Combat](https://codecombat.com/): un jeu en ligne, au gameplay
  assez bien pensé, où il faut résoudre de petites énigmes de
  programmation pour avancer. La liste des méthodes utilisables dépend
  de l'équipement découvert dans les différents niveaux. Au final,
  c'est graphiquement très plaisant, progressif à souhait, et assez
  agréable. Le "monde" gratuit demande quelques heures pour être
  résolu. 
- [CodinGame](https://www.codingame.com/): des défis de programmation
  visant des programmeurs déjà accomplis. Ce n'est pas forcément
  pédagogique (si on sait pas programmer, on ne peut pas commencer),
  mais c'est extrêmement ludique quand on sait un peu programmer. Les
  meilleurs compétiteurs du site se voient offrir des offres d'emplois
  qui financent le site.

## Activités de recherche guidée

L'idée est de mettre les stagiaires dans la position du chercheur, en
leur donnant un problème mathématique ou algorithmique, et en les
guidant dans leurs recherches sur plusieurs jours. On peut imaginer
avoir une session d'une heure ou deux chaque jour du stage pour
avancer sur le problème. 

Il existe de nombreux problèmes qu'on peut utiliser dans ce cadre.
[MathEnJean](https://www.mathenjeans.fr) est une véritable mine de
sujets potetiels. En voici quelques uns qui nous semblent intéressants,
issus d'autres sources.

- [Hexahexaflexagon](https://teachinglondoncomputing.org/computational-thinking-hexahexaflexagon-automata/)
  un objet matériel facile à fabriquer, et qui nécessite de construire
  un automate pour comprendre comment il marche. La doc est en
  anglais, mais c'est pour l'encadrant, pas pour les stagiaires.
  
- Le problème classique de l'[arbre recouvrant
  minimal](https://classic.csunplugged.org/minimal-spanning-trees/) se prête
  bien à l'exercice car c'est un problème certes trop difficile à
  formaliser pour des collégiens, mais que l'on peut les aider à
  résoudre "avec les mains, au tableau". Source d'instances
  intéressantes: [un partiel de L3](http://dept-info.labri.fr/~baudon/Licence/Algo2/Cours/Annales/exam-mai12.pdf)
  :)
  
  Un autre avantage de ce problème est qu'il existe un [article
  interstice](https://interstices.info/le-plus-court-chemin/)
  résolvant le cas général. L'article ne couvre pas certaines
  extensions, comme les graphes avec des arrêtes de poids minimal.
  C'est l'occasion de suivre le cycle du métier de chercheur, en
  alternant les phases de recherche pure et les phases de biblio avant
  d'adapter les solutions existantes à de nouveaux cas.

- On peut également utiliser différents algorithmes de flots. La plus
  grosse question est de trouver des instances du problème.

## Sites de vulgarisation
Donner un article de vulgarisation sur un sujet à lire, puis en discuter.
- [Interstices](https://interstices.info)
- Un numéro de Phosphore (magazine destiné aux lycéens) réalisé en
 partenariat avec Inria sur les sciences du numérique  
https://interstices.info/wp-content/uploads/jalios/metiers/inria-phosphore.pdf   

<img src="Phosphore1.jpg" width="400"/>
<img src="Phosphore2.jpg" width="400"/>

- Un numéro de Phosphore - Okapi en partenariat avec Inria sur les idées
reçues des sciences du numérique  
https://wiki.inria.fr/wikis/mecsci/images/9/9f/Phosphokapi-2012-A4.pdf


<img src="Phosphokapi.jpg" width="400"/>


Faire une activité clé en main
- [pixees](https://pixees.fr/des-ressources-pour-le-scolaire-niveau-college-cycle-4-5eme-4eme-3eme/)

Histoire de l'informatique  
https://www.histoire-informatique.org/grandes_dates/  

Des liens sur plein de sujets  
https://wiki.inria.fr/sciencinfolycee/Pour_préparer,_illustrer_des_cours  
https://www.inria.fr/recherches/mediation-scientifique/actions-de-mediation-scientifique/ressources#Des_activités_débranchées  

## Sites de présentation des métiers du numérique
- exemples de métiers  
https://wiki.inria.fr/sciencinfolycee/Exemples_de_métiers_du_numérique  
- la brochure Onisep sur les métiers des mathématiques, de la statistique et de l'informatique  
https://www.onisep.fr/content/download/769765/file/ZOOM-MATHS_partenaires.pdf

## Sites pour des ateliers d'informatique débranchée
- des activités expliquées en video [ici](https://members.loria.fr/MDuflot/files/mediation.html)

## Livret à donner
- en cours de construction...

# Remarques finales
Comments welcome!

